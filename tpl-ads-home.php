<?php
// Template Name: Ads Home Template
?>
<?php 
	get_template_part( 'featured' ); 
?>

<img id="advertise-here" src="<?php echo get_stylesheet_directory_uri().'/images/adveritise-here-arrow.png' ?>" />

<div class="content">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-8">
			<div id="classified-dirs">
				<?php	
					if (have_posts()) {
						while ( have_posts() ) : the_post();
							// Spit out the body content of the page (links to all of the ad categories or "Classified Directory",
							// "Classifieds by Category" page in wpadmin)
							the_content();
						endwhile;
					}
				?>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php if ( is_active_sidebar( 'pre_blog_posts_widget' ) ) : ?>
						<div id="pre-blog-widget-area" role="complementary">
							<?php dynamic_sidebar( 'pre_blog_posts_widget' ); ?>
						</div><!-- #primary-sidebar -->
					<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php
						// Get the Blog rss feed and insert it into the page
						$feedUrl = 'http://blog.usa4sale.net/category/ocala/feed/';
						$rawFeed = file_get_contents($feedUrl);
						$xml = new SimpleXmlElement($rawFeed);
						
						
					
						$args = array(
							'post_type' => 'post',
							'post_status' => 'publish',
							'posts_per_page' => '4' 
						);

						$the_query = new WP_Query( $args );

						echo '<div id="blog-posts">';
						
						// The Loop
						$loopIteration = 0;
						foreach ($xml->channel->item as $blogPost) {
							$postTitle  = $blogPost->title;
							$postUrl    = $blogPost->link;
							$postDate   = new DateTime($blogPost->pubDate);
							$postImage  = "http://bci.zone/os4hp/wp-content/uploads/2015/02/kitten-100x63.jpg";
							$postDescription = $blogPost->description;
							
							$dc = $blogPost->children('http://purl.org/dc/elements/1.1/');
							$postAuthor = $dc->creator;
							
							$content = 'http://purl.org/rss/1.0/modules/content/';
							$postContent = $blogPost->children($content)->encoded;
							
							
							?>
							<h3><a href="<?php echo $postUrl; ?>"><?php echo $postTitle; ?></a></h3>
							<div class="row post-info hidden-xs">
<!--								<div class="post-image">
									<?php 
										if ( $postImage != "" ) { // check if the post has a Post Thumbnail assigned to it.
											echo '<img width="100" height="63" src="'.$postImage.'" class="attachment-post-thumbnail wp-post-image">';
										} 
									?>
								</div>-->
								<?php echo $postContent; ?>
							</div>

							<?php
							$loopIteration ++;
							if ($loopIteration == 2) {
								if ( is_active_sidebar( 'inside_blog_posts_widget' ) ) {
									echo '<div id="homepage-blog-ads-widget-area">';
									dynamic_sidebar( 'inside_blog_posts_widget' );
									echo '</div>';
								}
							}
							else if ($loopIteration == 3) {
								break;
							}
						}
						echo '</div>';
					?>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h2 id="recent-ads-anchor" class="col-md-8 heading-dotted-bottom-border">Recently Published Classifieds</h2>
				</div>
				<div id='recently-published-classifieds-advertisement-wrapper' class="col-lg-4 visible-lg">
					<?php dynamic_sidebar( 'ad-sidebar' ); ?>
				</div>
				<div id="recently-published-classifieds" class="col-xs-12 col-lg-8">
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4">
			<?php dynamic_sidebar( 'main-sidebar' ); ?>
			<div id="local-sponsors-wrapper" class="thin-border hidden-xs">
				<h3 id="local-sponsors-header">Local Sponsors:</h3>
				<div class="tabcontrol">
						<ul class="tabnavig">
							<li><a href="#block1"><span class="big"><?php _e( 'A-G', APP_TD ); ?></span></a></li>
							<li><a href="#block2"><span class="big"><?php _e( 'H-M', APP_TD ); ?></span></a></li>
							<li><a href="#block3"><span class="big"><?php _e( 'N-Z', APP_TD ); ?></span></a></li>
						</ul>
						<?php
							remove_action( 'appthemes_after_endwhile', 'cp_do_pagination' );
							$post_type_url = add_query_arg( array( 'paged' => 2 ), get_post_type_archive_link( APP_POST_TYPE ) );
						?>
						<!-- tab 1 -->
						<div id="block1">
							<div class="clr"></div>
							<div class="undertab">
								<?php
									$page = get_posts(
												array(
													'name'      => 'local-sponsors-a-g',
													'post_type' => 'page'
												)
											);
									echo $page[0]->post_content;
								?>
							</div>
							<div class="clr"></div>
						</div><!-- /block1 -->
						<!-- tab 2 -->
						<div id="block2">
							<div class="clr"></div>
							<div class="undertab">
								<?php
									$page = get_posts(
												array(
													'name'      => 'local-sponsors-h-m',
													'post_type' => 'page'
												)
											);
									echo $page[0]->post_content;
								?>
							</div>
							<div class="clr"></div>
						</div><!-- /block2 -->
						<!-- tab 3 -->
						<div id="block3">
							<div class="clr"></div>
							<div class="undertab">
								<?php
									$page = get_posts(
												array(
													'name'      => 'local-sponsors-n-z',
													'post_type' => 'page'
												)
											);
									echo $page[0]->post_content;
								?>
							</div>
							<div class="clr"></div>
						</div><!-- /block3 -->
					</div><!-- /tabcontrol -->
				</div>
		</div>
	</div>
</div><!-- /content -->